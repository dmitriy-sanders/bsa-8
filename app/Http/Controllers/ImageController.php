<?php

declare(strict_types=1);

namespace App\Http\Controllers;

use App\Jobs\ImageJob;
use App\Services\AuthService;
use Illuminate\Http\Request;
use App\Values\Image;
use App\Actions\Image\{
    ApplyFilterAction,
    GetFiltersAction,
};

class ImageController extends Controller
{
    private ApplyFilterAction $applyFilter;
    private GetFiltersAction $getFilters;
    private AuthService $authService;

    public function __construct(
        ApplyFilterAction $applyFilter,
        GetFiltersAction $getFilters,
        AuthService $authService
    ) {
        $this->applyFilter = $applyFilter;
        $this->getFilters = $getFilters;
        $this->authService = $authService;
    }

    public function updateImage(Request $request)
    {
        $image = new Image(
            $request->imageId,
            $request->src,
        );


        ImageJob::dispatch(
            $image,
            $request->filter,
            $this->authService->getUser()
        );

        return response()->json([
            "status" => "success",
            $image->toArray(),
        ], 200);
    }

    public function filters()
    {
        return response()->json(
            $this->getFilters->execute(),
            200
        );
    }
}
