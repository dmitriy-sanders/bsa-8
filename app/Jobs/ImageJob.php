<?php

namespace App\Jobs;

use App\Entities\User;
use App\Events\ApplyFilterEvent;
use App\Notifications\ImageProcessedNotification;

use App\Notifications\ImageProcessingFailedNotification;
use App\Services\ImageApiService;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class ImageJob implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    private Image $image;
    private string $filter;
    private User $user;

    /**
     * Create a new job instance.
     *
     * @param Image $image
     * @param string $filter
     * @param User $user
     */
    public function __construct(
        Image $image,
        string $filter,
        User $user
    ) {
        echo $image->getSrc();
        $this->image = $image;
        $this->filter = $filter;
        $this->user = $user;
    }

    /**
     * Execute the job.
     *
     * @param ImageApiService $service
     * @return void
     */
    public function handle(ImageApiService $service)
    {
        $service->applyFilter($this->image->getSrc(), $this->filter);

        $notificationSuccess = new ImageProcessedNotification($this->image);

        ApplyFilterEvent::broadcast(
            $notificationSuccess->toBroadcast()
        );
    }

    /**
     * Handle a job failure.
     *
     * @param  \Exception  $exception
     * @return void
     */
    public function failed(\Exception $exception)
    {
        $notificationFail = new ImageProcessingFailedNotification($exception, $this->image, $this->user, $this->filter);

        ApplyFilterEvent::broadcast(
            $notificationFail->toBroadCast()
        );

        $this->user->notify($notificationFail);
    }
}
