<?php

namespace App\Notifications;

use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImageProcessedNotification extends Notification
{
    use Queueable;

    public string $imageId;
    public string $imageUrl;

    /**
     * Create a new notification instance.
     *
     * @param Image $image
     */
    public function __construct(Image $image)
    {
        $this->imageId = $image->getId();
        $this->imageUrl = $image->getSrc();
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['broadcast'];
    }

    public function toBroadcast()
    {
        return new BroadcastMessage([
            "status" => "success",
            "image" => [
                "id" => $this->imageId,
                "src" => $this->imageUrl
            ]
        ]);
    }
}
