<?php

namespace App\Notifications;

use App\Entities\User;
use App\Values\Image;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Notifications\Messages\BroadcastMessage;
use Illuminate\Notifications\Messages\MailMessage;
use Illuminate\Notifications\Notification;

class ImageProcessingFailedNotification extends Notification
{
    use Queueable;

    public \Exception $exception;
    public string $imageId;
    public string $imageUrl;
    public User $user;
    public string $filter;


    /**
     * Create a new notification instance.
     *
     * @param \Exception $exception
     * @param Image $image
     * @param User $user
     * @param string $filter
     */
    public function __construct(
        \Exception $exception,
        Image $image,
        User $user,
        string $filter
    ) {
        $this->exception = $exception;
        $this->imageId = $image->getId();
        $this->imageUrl = $image->getSrc();
        $this->user = $user;
        $this->filter = $filter;
    }

    /**
     * Get the notification's delivery channels.
     *
     * @param  mixed  $notifiable
     * @return array
     */
    public function via($notifiable)
    {
        return ['mail', 'broadcast'];
    }

    /**
     * Get the mail representation of the notification.
     *
     * @param  mixed  $notifiable
     * @return \Illuminate\Notifications\Messages\MailMessage
     */
    public function toMail()
    {
        return (new MailMessage)
                    ->line("Dear " . $this->user->name . PHP_EOL)

                    ->line("The applying the filter '".
                        $this->filter
                        ."' was failed to the image:" . PHP_EOL)

                    ->line("<a href='" .
                        $this->imageUrl . "'><img src='".
                        $this->imageUrl ."' alt='Image'></a>" . PHP_EOL)
            
                    ->line("Best regards,")
                    ->line("Binary Studio Academy");
    }


    public function toBroadCast()
    {
        return new BroadcastMessage([
            "status" => "failed",
            "message" => $this->exception->getMessage(),
            "image" => [
                "id" => $this->imageId,
                "src" => $this->imageUrl
            ]
        ]);
    }
}
